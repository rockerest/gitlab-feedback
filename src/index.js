import { isAuthenticated } from "./js/common/authentication.js";
import { appInstance } from "./js/common/bootstrap.js";

import { Authenticate } from "./js/components/authenticate/authenticate.js";
import { Feedback } from "./js/components/feedback/feedback.js";

async function start(){
	var output = document.getElementById( "feedback" );
	var app = await appInstance();

	customElements.define( Feedback.as, Feedback );
	customElements.define( Authenticate.as, Authenticate );

	output.insertAdjacentHTML( "beforeend", "<feedback-authenticate></feedback-authenticate>" );

	if( isAuthenticated() ){
		output.insertAdjacentHTML( "beforeend", "<feedback-feedback></feedback-feedback>" );
	}
}

start();
