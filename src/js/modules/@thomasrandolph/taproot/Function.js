async function pipe( ...args ){
	var first = args.shift();

	if( typeof first == "function" ){
		first = first();
	}

	return await( args.reduce( async ( prev, next ) => {
		return next( await prev );
	}, first ) );
}

export { pipe };
