import { get } from "../modules/@thomasrandolph/taproot/storage/local.js";

import { message as FEEDBACK_SEND } from "../messages/FEEDBACK_SEND.js";

export function register( { subscribeWith } = {} ){
	subscribeWith( {
		[FEEDBACK_SEND.name]: async ( event ) => {
			let { content, anonymous } = event;
			let token = get( "token" );

			await fetch( "/.netlify/functions/submitFeedback", {
				"method": "POST",
				"body": JSON.stringify( { anonymous, content, token } )
			} );

			alert( "Thanks for sending feedback!" );

			window.location.reload();
		}
	} );
}
