import { Component } from "../modules/@thomasrandolph/taproot/Component.js";

import { base } from "../common/styles.js";

export class BaseComponent extends Component{
	static get styles(){
		return [
			base()
		];
	}
}
