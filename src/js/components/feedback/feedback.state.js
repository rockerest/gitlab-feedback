import { getState as rootState, getService, applyToComponent } from "../../modules/@thomasrandolph/taproot/State.js";

export var states = {
	"waiting": {}
};

export function getState( { component, id, initial, propertyName = "state", serviceName = "stateService" } = {} ){
	var machine = rootState( {
		"id": id || `feedback-${component.uuid}`,
		"initial": initial || "waiting",
		states
	} );
	var service = getService( machine );

	applyToComponent( component, service, propertyName, serviceName );
	service.start();

	return service;
}
