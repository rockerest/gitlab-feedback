import { css } from "../../modules/@thomasrandolph/taproot/Component.js";

export function styles(){
	return css`

	div{
		display: grid;
		gap: 0.5rem;
		grid-template-columns: fit-content( 1rem ) 1fr max-content;
		grid-template-rows: 1fr fit-content( 1rem );
		height: 60vh;
		margin: auto;
		place-items: center;
		width: 50vw;
	}

	textarea{
		grid-column: 1 / 4;
		height: 100%;
		padding: 0.5rem;
		width: 100%;
	}

	label{
		justify-self: start;
	}

	`;
}
