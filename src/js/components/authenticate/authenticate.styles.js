import { css } from "../../modules/@thomasrandolph/taproot/Component.js";

export function styles(){
	return css`

	div{
		display: grid;
		gap: 0.5rem;
		margin: auto;
		width: 50vw;
	}

	div[authenticated]{
		grid-template-columns: 1fr max-content;
		place-items: right;
	}

	div[unauthenticated]{
		grid-template-columns: 1fr 5fr 1fr;
		place-items: center;
	}

	button[logout],
	button[login]{
		grid-column: 2;
	}

	section{
		grid-column: 1 / 4;
		grid-row: 2;
	}

	`;
}
