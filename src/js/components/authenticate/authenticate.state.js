import { getState as rootState, getService, applyToComponent } from "../../modules/@thomasrandolph/taproot/State.js";

export const FAILURE_EVENT = "FAILURE";
export const AUTHENTICATE_EVENT = "AUTHENTICATE";
export const RETRY_EVENT = "RETRY";
export const CLEAR_EVENT = "CLEAR";

export var states = {
	"waiting": {
		"on": {
			[AUTHENTICATE_EVENT]: "authing"
		}
	},
	"authing": {
		"on": {
			[FAILURE_EVENT]: "failed"
		}
	},
	"failed": {
		"on": {
			[CLEAR_EVENT]: "waiting",
			[RETRY_EVENT]: "authing"
		}
	}
};

export function getState( { component, id, initial, propertyName = "state", serviceName = "stateService" } = {} ){
	var machine = rootState( {
		"id": id || `authenticate-${component.uuid}`,
		"initial": initial || "waiting",
		states
	} );
	var service = getService( machine );

	applyToComponent( component, service, propertyName, serviceName );
	service.start();

	return service;
}
