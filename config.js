export var config = {
	"namespace": "feedback",
	"appId": "8d9fdab640c61ff0e8dcef3aebb0d329d1865f072a38166c2f29f80b65dd4549",
	"appSecretVar": {
		"type": "env",
		"name": "GITLAB_FEEDBACK_APP_SECRET"
	},
	"snippetTokenVar": {
		"type": "env",
		"name": "GITLAB_FEEDBACK_THOMAS_TOKEN"
	},
	"snippetId": 2253479,
	"feedbackFile": "feedback.txt",
	"extraEmails": [
		"gitlab@tomr.email"
	]
};
