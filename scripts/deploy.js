import { readFileSync, writeFileSync } from "fs";

import { emptyDirSync, copySync } from "fs-extra";
import { globbySync } from "globby";

var importMap = readFileSync( "./src/js/modules/import-map.json", "utf8" );
var files;

emptyDirSync( "./public" );
copySync(
	"./src/",
	"./public/"
);

files = globbySync( [
	"./public/index.html",
	"./public/views/**/index.html"
] );

importMap = importMap.replace( /": "\.\//gmi, '": "/js/modules/' );

files.forEach( ( file ) => {
	let contents = readFileSync( file, "utf8" );

	contents = contents.replace( "%%importmap%%", `<script type="importmap">\n${importMap}\n</script>` );

	writeFileSync( file, contents, "utf8" );
} );
